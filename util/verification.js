const jwtSecret = require('../config').secret;
const jwt = require('jsonwebtoken');

const VerifyToken = async (req, res, next) => {
    const authHeader = req.headers['authorization'];  //Read the contents of the authorization header
    if (!authHeader) return res.sendStatus(403);                                                         
    
    jwt.verify(authHeader, jwtSecret.login, function (err, decoded) {  //Middleware function to validate the token
        if (!decoded) return res.sendStatus(403);                  
        res.token = authHeader;                         //Token is valid so we set it in the response
        next();                                         //Move onto the next function
    });
}

module.exports = VerifyToken;