const Misc = {
    AddObjectContents(currentObj, newObj) {
        if (Object.keys(currentObj).length != Object.keys(newObj).length) return null;
        let updatedObj = {};

        for (i = 0; i < Object.keys(currentObj).length; i++) {
            let item = Object.values(currentObj)[i] += Object.values(newObj)[i];
            updatedObj[i + 1] = item;
        };
        return JSON.stringify(updatedObj);
    },
    GenerateDefaultObject(fieldcount) {
        let obj = {};
        for (i = 0; i < fieldcount; i++) obj[i + 1] = 0;
        return JSON.stringify(obj);
    }
};

module.exports = Misc;