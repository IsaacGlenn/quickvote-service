const jwtSecret = require('../config').secret;
const bcrypt = require('bcrypt');
const passport = require('passport'),
    localStrategy = require('passport-local').Strategy,
    User = require('../models/userModel'),
    JWTStrategy = require('passport-jwt').Strategy,
    ExtractJWT = require('passport-jwt').ExtractJwt;


passport.use(
    'register', new localStrategy({        //Setting up a new local strategy for registering an account
        usernameField: 'username',
        passwordField: 'password',
        session: false
    }, (username, password, first_name, last_name, email_address, access_level, creation_date, done) => {
        try {
            User.findOne({ username: username }).then(user => {   //Search for a user with this name in the database
                if (user) {                  //If a user exists
                    console.log('Username already exists.');
                    return done(null, false, { message: 'Username already exists.' }) //Callback with the error message
                }
                else {                      //If no user with this name exists
                    bcrypt.hash(password, 10).then(hashedPassword => {  //Hash the password
                        let user = new User();
                        user.username = username;                   // Setting the user attributes
                        user.password = hashedPassword;             //
                        user.first_name = first_name;               //
                        user.last_name = last_name;                 //
                        user.email_address = email_address;         //
                        user.access_level = access_level;           //
                        user.creation_date = creation_date;         //

                        user.save().then(user => {                      //Save the user to the database
                            console.log("User created successfully.");
                            return done(null, user);            //Callback with the user object
                        });
                    });
                }
            });
        } catch (err) {   //If an error occurs
            done(err);  //Callback with the error
        }
    },
    ),
);

passport.use(
    'login', new localStrategy({    //Defining a new local strategy for logging into an account
        usernameField: 'Username',
        passwordField: 'Password',
        session: false,
    }, (username, password, done) => {
        try {
            User.findOne({ where: { Username: username } }).then(user => {
                if (!user) return done(null, false, { message: 'Username does not exist' });
                if (!user.IsVerified) return done(null, false, { message: 'Account is not verified' })
                bcrypt.compare(password, user.Password).then(res => {
                    if (!res) return done(null, false, { message: 'Incorrect password.' });
                    return done(null, user);
                });
            });
        } catch (err) {
            done(err);
        }
    })
)

const opts = {
    jwtFromRequest: ExtractJWT.fromAuthHeaderWithScheme('JWT'),
    secretOrKey: jwtSecret.login   //Secret key from the jwtConfig file
};

passport.use(
    'jwt', new JWTStrategy(opts, (jwt_payload, done) => {   //Defining a new JWT strategy using the options defined above
        try {
            User.findOne({ username: jwt_payload.id }).then(user => { //Find the user in the passport database
                if (user) {               //If the user exists
                    done(null, false);  //Callback with false
                }
            });
        } catch (err) {       //If an error occurs
            done(err);      //Callback with error
        }
    })
);

passport.serializeUser(function (user, done) {
    done(null, user);
});

passport.deserializeUser(function (user, done) {
    done(null, user);
});