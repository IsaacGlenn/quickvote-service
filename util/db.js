const cfg = require('../config');
const { Sequelize } = require('sequelize');

const sequelize = new Sequelize(cfg.db.dbname, cfg.db.user, cfg.db.password, {
    host: cfg.db.host,
    dialect: cfg.db.dialect
});

sequelize.authenticate().then(() =>
    console.log('Connection has been established successfully.')
).catch(err => console.error('Unable to connect to the database: ', err));

module.exports = sequelize;