var express = require('express');
var router = express.Router();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const passport = require('passport');
require('../util/passport');
const cfg = require('../config');
const { Op } = require("sequelize");
const mailer = require('../util/mail');

const verifyToken = require('../util/verification');
const secret = require('../config').secret;
const myUser = require('../models/userModel');



router.get('/all', verifyToken, function (req, res, next) {
    myUser.findAll()
        .then(result => {
            res.json(result);
        })
        .catch(err => console.log(err));
});

router.get('/view/:id', verifyToken, function (req, res) {
    const id = req.params.id;
    return myUser.findOne({where: {id: id}})
    .then((user) => {
        return res.json(user);
    }).catch((err) => res.send({error: err}));
});

router.put('/register', async function (req, res, next) {
    let uname = req.body.Username.toString().toLowerCase();
    let email = req.body.EmailAddress.toString().toLowerCase();
    let fname = req.body.Firstname.toString().toLowerCase();
    let lname = req.body.Lastname.toString().toLowerCase();

    await myUser.findOne({
        where: {
            [Op.or]: [{ Username: uname }, { EmailAddress: email }]
        }
    }).then(found => {
        if (!found) {
            let today = new Date;
            bcrypt.hash(req.body.Password, 10, async function (err, hash) {
                myUser.create({
                    FirstName: fname,
                    LastName: lname,
                    EmailAddress: email,
                    Username: uname,
                    Password: hash,
                    IsVerified: false,
                    IsAdmin: false,
                    Created: today
                }).then(user => {
                    const verifyToken = jwt.sign({
                        user: user.id,
                        exp: Date.now() / 1000 + (60 * 60 * 24) //1 day expire
                    }, secret.verify);

                    mailer.sendVerify(user.EmailAddress, verifyToken);
                    return res.status(200).send({ message: 'user created successfully - email verification sent' });
                }).catch(err => { return res.json(err) });
            })
        }
        else return res.status(200).send({ error: 'Username or email address already in use' });
    }).catch(err => console.log(err))
});

router.post('/login', function (req, res, next) {
    passport.authenticate('login',
        (err, user, info) => {
            user = {
                username: req.body.Username,
                password: req.body.Password
            };
            if (err) return res.send(err);
            if (info) return res.send(info.message);
            else {
                req.logIn(user, err => {
                    if (err) return res.json(err);
                    myUser.findOne({ where: { Username: user.username } }).then(u => {
                        const token = jwt.sign({ id: u.Username, exp: Math.floor(Date.now() / 1000 + (60 * 60)) }, secret.login);  //1 Hour expire
                        return res.status(200).send({
                            auth: true,
                            token: token,
                            id: u.id,
                            message: 'User ' + u.Username + ' logged in',
                        });
                    });
                });
            }
        })(req, res, next)
})

router.get('/confirmation/:token', function (req, res, next) {
    const token = req.params.token;
    jwt.verify(token, secret.verify, async function (err, decoded) {
        if (err) return res.status(200).send({message: err, success: false});
        if (!decoded) return res.status(200).send({message: 'token expired', success: false});

        myUser.findOne({ where: { id: decoded.user } }).then(user => {
            if(!user) return res.status(200).send({message: 'user no longer exists', success: false});
            if (user.IsVerified) return res.status(200).send({ message: 'user is already verified', success: false });
        });

        myUser.update({ IsVerified: true }, { where: { id: decoded.user } })
            .then(() => { return res.status(200).send({ message: 'user successfully verified', success: true }); })
            .catch(() => { return res.status(200).send({message: err, success: false}); });
    });
})

router.post('/forgot', async function (req, res, next) {
    const email = req.body.email;

    await myUser.findOne({
        where: { EmailAddress: email }
    }).then(found => {
        if (found) {
            const forgotToken = jwt.sign({
                user: found.id,
                exp: Date.now() / 1000 + (60 * 15) //15 min expire
            }, secret.forgot);

            mailer.sendForgot(found.EmailAddress, forgotToken);
        }
        return res.json('if address exists, email sent');
    }).catch(err => { return res.json(err) });
});

router.post('/update/:token', async function (req, res, next) {
    const token = req.params.token;
    const newPassword = req.body.password;

    jwt.verify(token, secret.forgot, async function (err, decoded) {
        if (err) return res.json(err);
        if (!decoded) return res.json('invalid token');

        bcrypt.hash(newPassword, 10, async function (err, hash) {
            if (err) return res.json(err)
            if (hash) {
                // await myUser.findOne({ where: { id: decoded.user } })        This doesnt work, a new hash is created even if the password is the same
                //     .then((user) => {
                //         if (user.Password === hash) return res.status(200).send({ message: 'new password cannot be old password' })
                //         console.log(user.Password)
                //         console.log(hash)
                //     })
                //     .catch((err) => { return res.json(err) });

                await myUser.update({ Password: hash }, { where: { id: decoded.user } })
                    .then(() => { return res.status(200).send({ message: 'password updated', success: true }) })
                    .catch((err) => { return res.json(err) });
            }
        })
    });
});

router.get('/validate/:token', async function (req, res) {
    const token = req.params.token;

    return await jwt.verify(token, secret.login, async function (err, decoded) {
        if (err) return res.send({success: false});
        if (!decoded) return res.send({success: false});
        return res.send({success: true});
    });
});

module.exports = router;