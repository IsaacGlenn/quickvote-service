var express = require('express');
var router = express.Router();

const myData = require('../models/pollDataModel');
const myPoll = require('../models/pollModel');
const Misc = require('../util/misc');

const verifyToken = require('../util/verification');

router.get('/active', async function (req, res, next) {
    console.log('enter')
    await myData.findAll({
        //where: { PollID: req.params.id },
        include: {
            model: myPoll,
            where: { IsActive: true }
        }
    })
        .then(found => {
            console.log('success')
            return res.json(found);
        }).catch(err => res.json(err));
    console.log('end')
});

router.get('/id/:id', async function (req, res, next) {
    await myData.findOne({ where: { pollID: req.params.id } })
        .then(found => {
            return res.json(found);
        }).catch(err => res.json(err));
});

router.post('/id/:id', async function (req, res, next) {
    console.log(req.body);
    let newData = req.body.PollJson;
    let currentData = await myData.findOne({
        where: { PollID: req.params.id }
    }).then(item => {
        return item.dataValues.PollJson;
    });
    let newObj = Misc.AddObjectContents(newData, JSON.parse(currentData));
    console.log(newData);
    console.log(currentData);
    
    if(!newObj) return res.json('error');
    
    console.log(newObj);

    await myData.update({ PollJson: newObj }, { where: { PollID: req.params.id } })
        .then(res => {
            return res.json('success');
        }).catch(err => res.json(err));
});

router.put('/new', async function (req, res, next) {
    let obj = Misc.GenerateDefaultObject(req.body.FieldCount);
    await myData.create({
        PollID: req.body.PollID,
        PollJson: obj,
    }).then(poll => { return res.json(poll) })
        .catch(err => res.json(err));
})

module.exports = router;