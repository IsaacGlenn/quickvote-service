var express = require('express');
var router = express.Router();

const myPoll = require('../models/pollModel');
const verifyToken = require('../util/verification');

router.get('/active', async function (req, res, next) {
    await myPoll.findAll({ where: { IsActive: true } })
        .then(found => {
            return res.json(found);
        }).catch(err => res.json(err));
});

router.get('/id/:id', async function (req, res, next) {
    await myPoll.findOne({ where: { id: req.params.id } })
        .then(found => {
            return res.json(found);
        }).catch(err => res.json(err));
});

router.put('/new', function (req, res, next) {
    let today = new Date;
    let au = new Date(today.getTime() + (req.body.Duration*60000));

    myPoll.create({
        PollTitle: req.body.PollTitle,
        PollContent: req.body.PollContent,
        IsActive: req.body.IsActive,
        PollCreated: today,
        PollEdited: today,
        ActiveUntil: au,
        CreatedBy: req.body.CreatedBy,
        PollDescription: req.body.PollDescription,
        AllowAnonymous: req.body.AllowAnonymous
    }).then(poll => { return res.json(poll) })
    .catch(err => {return res.json(err)});
});

router.post('/id/:id', verifyToken, function (req, res, next) {
    let today = new Date;
    myPoll.update({
        PollTitle: req.body.PollTitle,
        PollContent: req.body.PollContent,
        IsActive: req.body.IsActive,
        PollEdited: today,
        ActiveUntil: req.body.ActiveUntil
    }, { where: { id: req.params.id } })
    .then(poll => {return res.json(poll)})
    .catch(err => {return res.json(err)});
});

router.delete('/id/:id', verifyToken, function (req, res, next) {
    myPoll.destroy({where: {id: req.params.id}})
    .then(count => {
        if(count > 0) return res.json('Poll deleted')
        else res.json('deletion failed');
    }).catch(err => {return res.json(err)});
});

module.exports = router;