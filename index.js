const express = require('express');
// const session = require("express-session");
// const mysqlSession = require("express-mysql-session");
// const mysqlstore = mysqlSession(session);
// const connection = {
//     host: cfg.db.host,

// }
// const store = new mysqlstore({}, connection)
// const sessionMiddleware = session({
//     store,
//     resave: true,
//     saveUninitialized: true,
//     secret: cfg.secret.session
// });

const bodyParser = require('body-parser');
const cors = require("cors");
const cookieParser = require("cookie-parser");
const passport = require('passport');

const poll = require("./api/poll");
const pollData = require("./api/pollData");
const user = require("./api/user");

const app = express();
const router = express.Router();

app.use(cookieParser());
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(cors({
    origin: '*'
}));
app.use(passport.initialize());

app.set('port', 3000);
app.use('/', router);
app.use('/polls', poll);
app.use('/polldata', pollData);
app.use('/users', user)

app.listen(app.get("port"), () => {
    console.log('listening on port 3000')
});

router.route('/index').get((req, res) => {
    console.log('hit')
    res.sendStatus(200);
});