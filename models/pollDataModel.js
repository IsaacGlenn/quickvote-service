'use strict';

const Sequelize = require('sequelize');
const db = require('../util/db');

const modelDef = {
    PollID: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    PollJson: {
        type: Sequelize.STRING,
        allowNull: false
    }
};

const modelOptions = {
    timestamps: false
};

const Poll = db.define('pollValues', modelDef, modelOptions);

module.exports = Poll;