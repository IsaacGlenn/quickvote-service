'use strict';

const Sequelize = require('sequelize');
const db = require('../util/db');
const bcrypt = require('bcrypt');

const modelDef = {
    FirstName: {
        type: Sequelize.STRING,
        allowNull: false
    },
    LastName: {
        type: Sequelize.STRING,
        allowNull: false
    },
    EmailAddress: {
        type: Sequelize.STRING,
        allowNull: false
    },
    Username: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false
    },
    Password: {
        type: Sequelize.STRING,
        allowNull: false
    },
    IsVerified: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    },
    IsAdmin: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    },
    Created: {
        type: Sequelize.DATE,
        allowNull: false
    }
};

const modelOptions = {
    instanceMethods: {
        ComparePasswords: ComparePasswords
    },
    timestamps: false
    // hooks: {
    //     BeforeValidate: HashPassword
    // }
};

function ComparePasswords(password, callback){
    bcrypt.compare(password, this.password, function(error, isMatch) {
        if(error) {
            return callback(error);
        }

        return callback(null, isMatch);
    });
}

// function HashPassword(user) {
//     if(user.changed('password')) {
//         return bcrypt.hash(user.password, 10).then(function(password) {
//             user.password = password;
//         });
//     }
// }

const User = db.define('user', modelDef, modelOptions);

module.exports = User;