'use strict';

const Sequelize = require('sequelize');
const db = require('../util/db');

const modelDef = {
    PollTitle: {
        type: Sequelize.STRING,
        allowNull: false
    },
    PollDescription: {
        type: Sequelize.STRING,
        allowNull: false
    },
    PollContent: {
        type: Sequelize.STRING,
        allowNull: false
    },
    IsActive: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    },
    PollCreated: {
        type: Sequelize.DATE,
        allowNull: false
    },
    PollEdited: {
        type: Sequelize.DATE,
        allowNull: false
    },
    ActiveUntil: {
        type: Sequelize.DATE,
        allowNull: false
    },
    CreatedBy: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    AllowAnonymous: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    }
};

const modelOptions = {
    timestamps: false
};

const Poll = db.define('poll', modelDef, modelOptions);

module.exports = Poll;