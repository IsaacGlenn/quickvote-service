-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 02, 2023 at 06:38 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `quickvote`
--

-- --------------------------------------------------------

--
-- Table structure for table `polls`
--

CREATE TABLE `polls` (
  `id` int(11) NOT NULL,
  `PollTitle` varchar(32) NOT NULL,
  `PollContent` mediumtext NOT NULL,
  `IsActive` tinyint(1) NOT NULL,
  `PollCreated` datetime NOT NULL,
  `PollEdited` datetime NOT NULL,
  `ActiveUntil` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `PollDescription` text NOT NULL,
  `AllowAnonymous` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `polls`
--

INSERT INTO `polls` (`id`, `PollTitle`, `PollContent`, `IsActive`, `PollCreated`, `PollEdited`, `ActiveUntil`, `CreatedBy`, `PollDescription`, `AllowAnonymous`) VALUES
(25, 'tests', '[{\"index\":0,\"value\":\"1\"},{\"index\":1,\"value\":\"2\"},{\"index\":2,\"value\":\"3\"},{\"index\":3,\"value\":\"4\"},{\"index\":4,\"value\":\"5\"}]', 1, '0000-00-00 00:00:00', '2020-07-27 17:32:09', '0000-00-00 00:00:00', 34, 'description', b'0'),
(37, 'Numberz', '[{\"index\":0,\"value\":\"1\"},{\"index\":1,\"value\":\"2\"},{\"index\":2,\"value\":\"3\"},{\"index\":3,\"value\":\"4\"},{\"index\":4,\"value\":\"5\"}]', 1, '2021-01-08 21:40:07', '2021-01-08 21:40:07', '2021-01-08 22:40:07', 34, 'Vote for numbers', b'0'),
(45, 'Letters', '[{\"index\":0,\"value\":\"A\"},{\"index\":1,\"value\":\"B\"},{\"index\":2,\"value\":\"C\"},{\"index\":3,\"value\":\"D\"},{\"index\":4,\"value\":\"E\"}]', 1, '2021-01-09 18:37:06', '2021-01-09 18:37:06', '2021-01-09 21:37:06', 34, 'Vote for your favourite letter!', b'0');

-- --------------------------------------------------------

--
-- Table structure for table `pollvalues`
--

CREATE TABLE `pollvalues` (
  `id` int(11) NOT NULL,
  `pollID` int(11) NOT NULL,
  `pollJson` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pollvalues`
--

INSERT INTO `pollvalues` (`id`, `pollID`, `pollJson`) VALUES
(4, 25, '{\"1\":6,\"2\":12}'),
(8, 45, '{\"1\":1,\"2\":1,\"3\":1,\"4\":1,\"5\":1}');

-- --------------------------------------------------------

--
-- Table structure for table `test`
--

CREATE TABLE `test` (
  `id` int(255) NOT NULL,
  `column1` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `test`
--

INSERT INTO `test` (`id`, `column1`) VALUES
(1, 'test');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `Firstname` varchar(16) NOT NULL,
  `Lastname` varchar(16) NOT NULL,
  `EmailAddress` varchar(32) NOT NULL,
  `Username` varchar(16) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `IsVerified` tinyint(4) NOT NULL,
  `IsAdmin` tinyint(4) NOT NULL,
  `Created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `Firstname`, `Lastname`, `EmailAddress`, `Username`, `Password`, `IsVerified`, `IsAdmin`, `Created`) VALUES
(1, 'isaac', 'glenn', 'isaac@glenn.com', 'isaacg', '$2y$10$BZf6PaPzTMgVzzBs.8F0M.wC7mru5LoaVgmBSoFE4JxJVczpFBmI2', 0, 0, '0000-00-00 00:00:00'),
(34, 'jh', 'jh', 'isaacglenn@live.co.uk', 'glenbob', '$2b$10$kkk5PEnmp97mEnIrSYl5IeqtCxK/jTJ4d0M/MAkTD11pkxrWnvyke', 1, 0, '2020-08-19 18:57:21'),
(35, 'as', 'as', 'asas@asas.com', 'as', '$2b$10$wq1i2I30mFTwv/w9psUd4u00pB5q.rXpXXmblQDn3HdXe7M6gxHIG', 0, 0, '2020-08-20 12:57:49'),
(39, 'as', 'as', 'glenbob1337@gmail.com', 'asa', '$2b$10$sVOJxFLDSTlVK1jbc2MBJ.ISL3b4N96GWZ303oKguI4PgdynFW.DC', 1, 0, '2020-08-20 13:23:51'),
(40, 'sasasa', 'sasasa', 'sasassasa@asasasa.asaasa', 'saasa', '$2b$10$hyH4vICqL7YYBaXRL72m5uq7ZIlMs8oFbgYd0OZYRMXHDbA2f0h6K', 0, 0, '2023-06-13 15:41:59'),
(41, 'sasasa', 'sasasa', 'sasassasa@asasasa.asaasa', 'saasa', '$2b$10$1pVpQk.iPJWAc.PUeCv8HepS4ni99D9YtCThQRuMZpGboOGvKNRYC', 0, 0, '2023-06-13 15:41:59');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `polls`
--
ALTER TABLE `polls`
  ADD PRIMARY KEY (`id`),
  ADD KEY `CreatedBy` (`CreatedBy`);

--
-- Indexes for table `pollvalues`
--
ALTER TABLE `pollvalues`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pollID` (`pollID`);

--
-- Indexes for table `test`
--
ALTER TABLE `test`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `polls`
--
ALTER TABLE `polls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `pollvalues`
--
ALTER TABLE `pollvalues`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `test`
--
ALTER TABLE `test`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `polls`
--
ALTER TABLE `polls`
  ADD CONSTRAINT `polls_ibfk_1` FOREIGN KEY (`CreatedBy`) REFERENCES `users` (`id`);

--
-- Constraints for table `pollvalues`
--
ALTER TABLE `pollvalues`
  ADD CONSTRAINT `pollvalues_ibfk_1` FOREIGN KEY (`pollID`) REFERENCES `polls` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
